/*#include "ESP8266WiFi.h"

void setup() {
  Serial.begin(115200);
//   Serial.setDebugOutput(true);

  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  delay(100);

  Serial.println("");
  Serial.println("Setup done");
  Serial.println("");
  //Serial.println("MAC: " + WiFi.macAddress());
}

String encryptionTypeStr(uint8_t authmode) {
    switch(authmode) {
        case ENC_TYPE_NONE:
            return "OPEN";
          case ENC_TYPE_WEP:
            return "WEP";
        case ENC_TYPE_TKIP:
            return "WPA_PSK";
        case ENC_TYPE_CCMP:
            return "WPA2_PSK";
        case ENC_TYPE_AUTO:
            return "WPA_WPA2_PSK";
        default:
            return "UNKOWN";
    }
}

void loop() {
  //  Serial.println("scan start");

  // WiFi.scanNetworks will return the number of networks found
  int n = WiFi.scanNetworks(false,false);
  //  Serial.println("scan done");
  int o = n;
  int loops = 0;

  if (n == 0)
    Serial.println("no networks found");
  else
  {
    // sort by RSSI
    int indices[n];
    int skip[n];

    String ssid;

    for (int i = 0; i < n; i++) {
      indices[i] = i;
    }

    // CONFIG
    bool sortRSSI   = true; // sort aps by RSSI
    bool removeDups = false; // remove dup aps ( forces sort )
    bool printAPs   = true; // print found aps

    bool printAPFound = false; // do home ap check
    const char* homeAP = "MYAP"; // check for this ap on each scan
    // --------

    bool homeAPFound   = false;

    if(removeDups || sortRSSI){
      for (int i = 0; i < n; i++) {
        for (int j = i + 1; j < n; j++) {
          if (WiFi.RSSI(indices[j]) > WiFi.RSSI(indices[i])) {
            loops++;
            //int temp = indices[j];
            //indices[j] = indices[i];
            //indices[i] = temp;
            std::swap(indices[i], indices[j]);
            std::swap(skip[i], skip[j]);
          }
        }
      }
    }

    if(removeDups){
      for (int i = 0; i < n; i++) {
        if(indices[i] == -1){
          --o;
          continue;
        }
        ssid = WiFi.SSID(indices[i]);
        for (int j = i + 1; j < n; j++) {
          loops++;
          if (ssid == WiFi.SSID(indices[j])) {
            indices[j] = -1;
          }
        }
      }
    }

//    Serial.println((String)loops);
    Serial.print(o);
    Serial.println(" networks found of " + (String)n);

    //Serial.println("00: (RSSI)[BSSID][hidden] SSID [channel] [encryption]");
    for (int i = 0; i < n; ++i)
    {
      if(printAPFound && (WiFi.SSID(indices[i]) == homeAP)) homeAPFound = true;

      if(printAPs && indices[i] != -1){
      // Print SSID and RSSI for each network found
      Serial.printf("%02d", i + 1);
      Serial.print(":");

      Serial.print(" (");
      Serial.print(WiFi.RSSI(indices[i]));
      Serial.print(")");

//      Serial.print(" [");
//      Serial.print(WiFi.BSSIDstr(indices[i]));
//      Serial.print("]");

//      Serial.print(" [");
//      Serial.print((String) WiFi.isHidden(indices[i]));
//      Serial.print("]");

      Serial.print(" " + WiFi.SSID(indices[i]));
      // Serial.print((WiFi.encryptionType(indices[i]) == ENC_TYPE_NONE)?" ":"*");

//      Serial.print(" [");
//      Serial.printf("%02d",(int)WiFi.channel(indices[i]));
//      Serial.print("]");
//
//      Serial.print(" [");
//      Serial.print((String) encryptionTypeStr(WiFi.encryptionType(indices[i])));
//      Serial.print("]");

//      Serial.print(" WiFi index: " + (String)indices[i]);

      Serial.println();
      }
      delay(10);
    }
     if(printAPFound && !homeAPFound) Serial.println("HOME AP NOT FOUND");
     Serial.println("");
  }
  delay(500);
}*/

#include "ESP8266WiFi.h"

int AP_List_View_Bott = 14;

int AP_RSSI[50];
String AP_SSID[50];

char rx_byte = 0;
String rx_str = "", rx_ssid = "";

void setup() {

  pinMode(AP_List_View_Bott, INPUT); 

  Serial.begin(115200);

  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
}

void loop() {

  Serial.print("Scan starting...       ");
  // WiFi.scanNetworks will return the number of networks found
  int n = WiFi.scanNetworks();
  Serial.println("Scan done!");

  if (n == 0)
    Serial.println("no networks found");
  else
  {
    Serial.print(n);
    Serial.println(" networks found");
    Serial.println("Enter your AP SSID!");

    while ((Serial.available() > 0)) {   // get a specified AP SSID from Serial Motitor Window

      rx_byte = Serial.read();
      if (rx_byte != '\n') {
        // a character of the string was received
        rx_str += rx_byte;
      }
      else {
        // end of string
        Serial.print("Specified AP is ");
        Serial.println(rx_str);
        rx_ssid = rx_str;
        rx_str = "";
      }
    }

    if (digitalRead(AP_List_View_Bott) == HIGH) {      // if this button is pressed, show the AP list with the RSSI value.
      for (int i = 0; i < n; ++i) {
        AP_RSSI[i] = WiFi.RSSI(i);
        AP_SSID[i] = WiFi.SSID(i);
        Serial.print(AP_SSID[i]);
        Serial.print(" : ");
        Serial.println(AP_RSSI[i]);
      }
    }
    else {                             // if this button is not pressed, show only a specified AP SSID with RSSI.
      for (int i = 0; i < n; ++i) {
        AP_RSSI[i] = WiFi.RSSI(i);
        AP_SSID[i] = WiFi.SSID(i);
        if (AP_SSID[i] == rx_ssid) {
          Serial.print(AP_SSID[i]);
          Serial.print(" : ");
          Serial.println(AP_RSSI[i]);
        }
      }
    }
  }
  Serial.println("");
  // Wait a bit before scanning again
  delay(2000);
}

