#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include "ESP8266WiFi.h"
#include <Adafruit_ssd1306syp.h>

static const int RXPin = 12, TXPin = 13;
static const uint32_t GPSBaud = 9600;

int AP_List_View_Bott = 14;

int AP_RSSI[50];
String AP_SSID[50];

char rx_byte = 0;
String rx_str = "", rx_ssid = "";

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

Adafruit_ssd1306syp display(4, 5);

void setup() {

  pinMode(AP_List_View_Bott, INPUT);

  Serial.begin(115200);
  ss.begin(GPSBaud);

  // Set WiFi to station mode and disconnect from an AP if it was previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  display.initialize();
  display.clear();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
}

void loop() {

  Serial.print("Scan starting...       ");
  // WiFi.scanNetworks will return the number of networks found
  int n = WiFi.scanNetworks();
  Serial.println("Scan done!");

  if (n == 0)
    Serial.println("no networks found");
  else
  {
    Serial.print(n);
    Serial.println(" networks found");
    Serial.println("Enter your AP SSID!");

    while ((Serial.available() > 0)) {   // get a specified AP SSID from Serial Motitor Window

      rx_byte = Serial.read();
      if (rx_byte != '\n') {
        // a character of the string was received
        rx_str += rx_byte;
      }
      else {
        // end of string
        Serial.print("Specified AP is ");
        Serial.println(rx_str);
        rx_ssid = rx_str;
        rx_str = "";
      }
    }

    if (digitalRead(AP_List_View_Bott) == HIGH) {      // if this button is pressed, show the AP list with the RSSI value.

      display.clear();
      display.setCursor(0, 0);
      for (int i = 0; i < 7; ++i) {
        AP_RSSI[i] = WiFi.RSSI(i);
        AP_SSID[i] = WiFi.SSID(i);
        Serial.print(AP_SSID[i]);
        Serial.print(" : ");
        Serial.println(AP_RSSI[i]);

        display.print(AP_SSID[i]);
        display.print(":");
        display.println(AP_RSSI[i]);

        //display.update();
      }
      display.update();
    }
    else {                             // if this button is not pressed, show only a specified AP SSID with RSSI.

      /*// This sketch displays information every time a new sentence is correctly encoded.
      while (ss.available() > 0)
        if (gps.encode(ss.read()))
          displayInfo();

      if (millis() > 5000 && gps.charsProcessed() < 10)
      {
        Serial.println(F("No GPS detected: check wiring."));
        while (true);
      }*/

      for (int i = 0; i < n; ++i) {
        AP_RSSI[i] = WiFi.RSSI(i);
        AP_SSID[i] = WiFi.SSID(i);
        if (AP_SSID[i] == "TP-LINK-REAL3D") {
          Serial.print(AP_SSID[i]);
          Serial.print(" : ");
          Serial.println(AP_RSSI[i]);

          /*//display.clear();
          display.setCursor(0, 0);
          display.println();
          display.println();
          display.println();
          display.print(AP_SSID[i]);
          display.print(" : ");
          display.println(AP_RSSI[i]);

          display.update();*/
          // This sketch displays information every time a new sentence is correctly encoded.
          while (ss.available() > 0)
            if (gps.encode(ss.read()))
              displayInfo(AP_SSID[i], AP_RSSI[i]);

          if (millis() > 5000 && gps.charsProcessed() < 10)
          {
            Serial.println(F("No GPS detected: check wiring."));
            while (true);
          }
        }
      }
    }
  }
  Serial.println();
  // Wait a bit before scanning again
  delay(1000);
}

void displayInfo(String AP_SSID, int AP_RSSI)
{
  if (gps.location.isValid())
  {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);

    display.clear();
    display.setCursor(0, 0);
    display.print("Latitude: ");
    display.println(gps.location.lat(), 6);
    display.print("Longitude: ");
    display.println(gps.location.lng(), 6);
    display.println();
    display.print(AP_SSID);
    display.print(" : ");
    display.println(AP_RSSI);

    display.update();

    //display.update();
    //delay(1000);
  }
  else
  {
    display.clear();
    display.setCursor(0, 0);
    display.println("INVALID");    
    display.println();
    display.println();
    display.print(AP_SSID);
    display.print(" : ");
    display.println(AP_RSSI);

    display.update();
  }
  //Serial.println();
}
