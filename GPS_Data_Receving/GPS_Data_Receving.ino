#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <Adafruit_ssd1306syp.h>

static const int RXPin = 12, TXPin = 13;
static const uint32_t GPSBaud = 9600;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
SoftwareSerial ss(RXPin, TXPin);

Adafruit_ssd1306syp display(4, 5);

void setup()
{
  Serial.begin(115200);
  ss.begin(GPSBaud);  

  display.initialize();
  display.clear();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
}

void loop()
{
  // This sketch displays information every time a new sentence is correctly encoded.
  while (ss.available() > 0)
    if (gps.encode(ss.read()))
      displayInfo();

  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    Serial.println(F("No GPS detected: check wiring."));
    while (true);
  } 
}

void displayInfo()
{
  if (gps.location.isValid())
  {
    Serial.print(gps.location.lat(), 6);
    Serial.print(F(","));
    Serial.print(gps.location.lng(), 6);

    display.clear();
    display.setCursor(0, 0);
    display.print("Latitude: ");
    display.println(gps.location.lat(), 6);
    display.print("Longitude: ");
    display.println(gps.location.lng(), 6);

    display.update();
    delay(1000);
  }
  else
  {
    display.clear();
    display.setCursor(0, 0);
    display.print("INVALID");    
    display.update();
  }  
  Serial.println();
}
